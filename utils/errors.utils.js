module.exports.signUpErrors = (err) => {
  let errors = { pseudo: "", email: "", password: "" };

  if (err.message.includes("pseudo"))
    errors.pseudo = "Pseudo incorrect ou déjà pris";

  if (err.message.includes("email")) errors.email = "Email incorrect";
  if (err.message.includes("password"))
    errors.password = "Le mot de passe doit faire 6 caractère minimum";

  if (err.code === 11000 && Object.keys(err.keyValue)[0].includes("pseudo"))
    errors.pseudo = "Pseudo incorrect ou déjà pris";

  if (err.code === 11000 && Object.keys(err.keyValue)[0].includes("email"))
    errors.email = "cet email est déjà pris par quelqu'un d'autres";

  return errors;
};

module.exports.signInError = (err) => {
  let errors = { email: "", password: "" };
  if (err === "Incorrect email")
    errors.email = "L'address email ne correspond pas";

  if (err === "Incorrect password")
    errors.password = "Le mot de passe ne correspond pas";
  return errors;
};
